
QUEEN = "Q"
EMPTY = "."

VALID = True
INVALID = False

class Board:
    
    @staticmethod
    def from_repr(board_repr):
        lines = board_repr.splitlines()
        queens = []
        
        for line_index,line in enumerate(lines):
            for col_index, col in enumerate(line):
                if(col == QUEEN):
                    queens.append((line_index, col_index))
        
        return Board(queens, len(lines), len(lines[0]))

    def __init__(self, queens, lines, columns):
        self.queens = queens
        self.lines = lines
        self.columns = columns

    def check(self):
        return self.__check_lines() and self.__check_columns() and \
            self.__check_diagonals()

    def __check_lines(self):
        occupied_lines = []
        
        for (line, _) in self.queens:
            if line in occupied_lines:
                return INVALID
            occupied_lines.append(line)
        
        return VALID

    def __check_columns(self):
        occupied_columns = []

        for (_,col) in self.queens:
            if col in occupied_columns:
                return INVALID
            occupied_columns.append(col)

        return VALID

    def __check_diagonals(self):
        inaccessible_positions_in_diag = set()
        for queen in self.queens:
            if queen in inaccessible_positions_in_diag:
                return INVALID 
            diags = self.__generate_diagonals(queen)
            inaccessible_positions_in_diag = inaccessible_positions_in_diag.union(diags)
                            
        return VALID
    
    def __generate_diagonals(self, position):
        diagonals = []
        diagonals.extend(self.__generate_first_diagonals(position))
        diagonals.extend(self.__generate_second_diagonals(position))
        return diagonals
    
    def __generate_first_diagonals(self, position):
        (line, col) = position
        while line > 0 and col > 0:
            line = line - 1
            col = col - 1
            yield (line,col)
        
        (line, col) = position
        while line < self.lines and col < self.columns:
            line = line + 1
            col = col + 1
            yield (line,col)

       
    def __generate_second_diagonals(self, position):
        (line, col) = position
        while line > 0 and col < self.columns:
            line = line - 1
            col = col + 1
            yield (line,col)

        (line, col) = position
        while line < self.lines and col > 0:
            line = line + 1
            col = col - 1
            yield (line,col)