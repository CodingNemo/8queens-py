from queens import Board, VALID, INVALID

def test_that_board_is_valid_when_no_queen_attack_each_other():
    board_repr = \
        "Q..\n" + \
        "..Q\n" + \
        "...\n"

    board = Board.from_repr(board_repr)

    assert board.check() is VALID


def test_that_board_is_invalid_when_two_queens_on_same_line():
    board_repr = \
        "Q.Q\n" + \
        "...\n" + \
        "...\n"

    board = Board.from_repr(board_repr)

    assert board.check() is INVALID


def test_that_board_is_invalid_when_two_queens_on_same_column():
    board_repr = \
        ".Q.\n" + \
        "...\n" + \
        ".Q.\n"

    board = Board.from_repr(board_repr)

    assert board.check() is INVALID


def test_that_board_is_invalid_when_two_queens_on_same_first_diagonal():
    board_repr = \
        "...\n" + \
        "Q..\n" + \
        ".Q.\n"

    board = Board.from_repr(board_repr)

    assert board.check() is INVALID

def test_that_board_is_invalid_when_two_queens_on_same_second_diagonal():
    board_repr = \
        "..Q\n" + \
        ".Q.\n" + \
        "...\n"

    board = Board.from_repr(board_repr)

    assert board.check() is INVALID